# TP noté

Copies anonymisées (si vous souhaitez voir ce que vous aviez rendu), pur ceux qui ont respecté la convention de renommage de fichier.
Pour trouver votre copie il faut se souvenir de votre numéro de poste.

https://nextcloud.lisn.upsaclay.fr/index.php/s/YJkAGcwexif2Lfo

Détails des notes (barème détaillé), anonymisé, pour un bon nombre de salles:

https://nextcloud.lisn.upsaclay.fr/index.php/s/4SXyD4HZExzJBdn


Merci à Théo d'avoir fait tout ça !
