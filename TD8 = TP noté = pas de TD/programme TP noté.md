
# Programme du TP:

Bonjour,
Comme vous le savez, jeudi 7 novembre, à la rentrée des vacances, il y aura un TP noté en IAS.
Il comptera pour 30% de la note finale (20% en session 2).

Le format est le suivant :
- **individuel**,
- sur ordinateurs en mode examen donc **sans internet**,
- sous **linux** (si cela pose un problème majeur à nombre d'entre vous, il faut me le signaler, et on peut prévoir 1 salle en Windows.. mais je crois que peu d'entre vous l'utilisent- ça ne change pas grand chose pour passer le TP)
- en **temps limité** (**2h** -- attention en fin d'examen les session seront bloquées automatiquement, donc c'est une limite  "dure")
- **documents autorisés: 6 pages de notes manuscrites**, pas plus.
- il y aura aussi des **documents présents avec le sujet**:
    + les **slides de cours et certains corrigés de TP**,
    + qq pages de documentation (numpy ou sklearn) en html,
    + mais aucun de vos fichiers personnels ne sera accessible
- sans internet, donc, sans jupyterhub. Assurez-vous que vous savez lancer une instance locale de jupyter (typiquement via une commande  `jupyter-notebook3`/`jupyter notebook`/etc ou bien avec un IDE (**EDIT: mais a priori pas VSCode, qui ne sera a priori pas disponible, car nécessite des plugins à installer**) ou autre: bref, un logiciel qui soit installé localement sur toutes les machines du PUIO). Attention que **votre fichier de configuration local d'IDE ne sera pas disponible**, vous aurez les logiciels dans leur version "comme si ça venait d'être installé". Bref, préférez la version browser (ce qu'on obtient avec la commande `jupyter-notebook`)
- la répartition dans les salles vous sera communiqué le jour même (une liste avec vos noms et la salle attribuée), via le gitlab.
- la partie 4 (la dernière) est plutot facile, pensez à la faire tôt.
- du papier de brouillon sera distribué

Quelques conseils (ils seront rappelés dans le sujet):
- avant de terminer, **faire `Kernel > Restart & Run All`**. Si lorsque nous corrigeons, le code plante parce qu'il ne doit pas etre exécuté dans l'ordre ou il apparait, nous considérerons le code comme défaillant.
- renommez le fichier de rendu en y insérant vos nom et prénoms, et **inscrivez aussi vos nom et prénoms dans le fichier** (une case sera prévue).
- enregistrez fréquemment (ctrl+S). Si vous faites des copies, indiquez laquelle est a corriger, ou mieux, supprimer celles qui sont inutiles a la fin.
- prenez les questions dans l'ordre qui vous convient le mieux. Il y aura plusieurs parties indépendantes.
- lisez bien les questions. Ce sera assez guidé, mais il faut bien comprendre l'objectif de chaque question.
- **si le code met trop de temps à s'exécuter, ce n'est pas normal: interrompez l'exécution** (carré en haut, ou bien sinon,`Kernel>Shutdown`)
- **à la fin, faites `Kernel > Restart & Run All`, enregistrez votre travail et déconnectez vous, nous récupérerons vos travaux automatiquement.**
- n'éteignez pas la machine en parant ! Il faut qu'elles soient allumées pour qu'on puisse récupérer votre travail via le réseau.


Groupe 1: regardez l'EDT. Vous n'avez pas TP le jeudi 7 novembre matin, puisque vous passez le TP noté en même temps que les autres.

**Comment réviser ?**
Le TP sera dans l'esprit de ce qu'on a fait en TD et TPs, ni plus dur ni tellement plus facile, mais juste assez différent pour que ce ne soit pas du copié-collé.
Il pourra donc y avoir:
- un peu de calcul de gradient à faire à la main (pas vraiment du code, mais vérifié par le code ensuite).
- une méthode ".fit" à coder (une descente de gradient, dans l'esprit du Correction du 4.3 -- Perceptron-qq-iterations-correction-updated.ipynb , qui ressemble en fait aussi beaucoup à exo2.3-mini-TP-DescenteGradient.ipynb )
- une optimisation de 1 hyper-paramètre à faire: split en train+val+test, train, mesure des scores train et val, affichage des résultats sous forme de graphique (dans l'esprit de la partie 2 de PCA+SVM-parties1-2-3-enonce-ameliore-correction.ipynb ) Ceci, avec ou sans Cross-Validation, je ne sais pas encore (ce sera la surprise, p-e qu'il y aura les 2 questions, progressivement, comme dans le TP).
- analyser le résultat obtenu (dans l'esprit de *Analyse de résultats d’expérience pages 21-23.pdf*
- p-e une expérience comme présentée dans Analyse de résultats d’expérience pages 21-23.pdf , à analyser (mais la vous pourrez meme faire des modifications pour corriger les problèmes ou affiner la recherche).

Je crois avoir tout couvert. Il se peut qu'on utilise PCA ou feature maps, mais il n'y aurait a priori pas de questions théoriques dessus, ce serait juste des boites noires qu'on utilise.

Bonnes révisions !
