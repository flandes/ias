import numpy as np
import matplotlib.pyplot as plt

mu = (2,1)
cov = np.array([[1, 0], [-4, 10]])
N=1000
np.random.seed(42)
X = np.random.multivariate_normal(mu, cov, (N))

print(X, X.shape)

plt.figure(1,[8,8])
plt.scatter(X[:,0], X[:,1], marker='x', color='k')
plt.xlim([-10,10])
plt.ylim([-10,10])
# plt.savefig("GaussianCovariances_"+str(cov[0,1])+"_"+str(cov[1,0])+".svg")
# plt.savefig("GaussianCovariances_"+str(cov[0,1])+"_"+str(cov[1,0])+".pdf")
plt.savefig("GaussianCovariances_"+str(cov[0,1])+"_"+str(cov[1,0])+".png")
# plt.hline(0)
# plt.vline(0)
plt.show()
