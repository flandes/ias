On a:
- Des échantillons $x_1, \ldots, x_N$.
- Une loi paramétrique $f(x, \lambda) = \lambda \exp(-\lambda x)$.

On définit la vraisemblance de $f$ par rapport à l'échantillon $(x_i)_{i=1}^N$ par la formule
$$
V(\lambda)_{(x_i)_{i=1}^N} = \prod_{i=1}^N f(x_i, \lambda) = \prod_{i=1}^N \lambda \exp(-\lambda x_i)
$$

Le maximum de vraisemblance de $V(\lambda)_{(x_i)_{i=1}^N}$ est:
$$
\max_{\lambda\in\mathbb{R}_*^+} V(\lambda)_{(x_i)_{i=1}^N}
$$

Et on dénomme estimateur du maximum de vraisemblance de $\lambda$, un paramètre $\lambda^*\in\mathbb{R}_*^+$ tel que
$$
V(\lambda^*)_{(x_i)_{i=1}^N}=\max_{\lambda\in\mathbb{R}_*^+} V(\lambda)_{(x_i)_{i=1}^N}
$$

On définit la log vraisemblance comme la fonction:
\begin{align*}
lV(\lambda)_{(x_i)_{i=1}^N} &= -\log\left(V(\lambda)_{(x_i)_{i=1}^N}\right)=-\log\left(\prod_{i=1}^N f(x_i, \lambda)\right)\\
&= -\sum_{i=1}^N\left(\log(\lambda)+\underbrace{\log(\exp(-\lambda x_i))}_{=-\lambda x_i}\right) = -N\log(\lambda) + \lambda\sum_{i=1}^N x_i
\end{align*}

Le maximum de la vraisemblance $V(\lambda)_{(x_i)_{i=1}^N}$ est alors le minimum de la log-vraisemblance par stricte décroissance de la fonction -log. On cherche alors les points critiques de la log-vraisemblance:
\begin{align*}
 &\frac{d}{d\,\lambda} lV(\lambda)_{(x_i)_{i=1}^N}=-\frac{N}{\lambda} + \sum_{i=1}^N x_i=0\\
 \iff& \frac{N}{\lambda} = \sum_{i=1}^N x_i\iff \frac{1}{\lambda} = \frac{1}{N}\sum_{i=1}^N x_i\\
 \iff& \lambda=\frac{1}{\frac{1}{N}\sum_{i=1}^N x_i}
\end{align*}
Le seul point critique de $lV(\cdot)_{(x_i)_{i=1}^N}$ est donc $\lambda^*=\frac{1}{\frac{1}{N}\sum_{i=1}^N x_i}$. Il faut encore vérfier que ce point critique est un minimum. Pour cela étudions le signe de la dérivée de $lV(\cdot)_{(x_i)_{i=1}^N}$:
- Si $\lambda<\lambda^*$, alors $\frac{1}{\lambda^*}<\frac{1}{\lambda}$, d'où $-\frac{N}{\lambda}<-\frac{N}{\lambda^*}$ et donc $\frac{d}{d\,\lambda} lV(\lambda)_{(x_i)_{i=1}^N}=-\frac{N}{\lambda}+\sum_{i=1}^N x_i<-\frac{N}{\lambda^*}+\sum_{i=1}^N x_i=\frac{d}{d\,\lambda} lV(\lambda^*)_{(x_i)_{i=1}^N}=0$. Ainsi la dérivée de $lV(\cdot)_{(x_i)_{i=1}^N}$ est strictement négative sur $]0,\lambda^*[$ et donc $lV(\cdot)_{(x_i)_{i=1}^N}$ est strictement décroissante sur $]0,\lambda^*[$.
- Si $\lambda>\lambda^*$, le même raisonnement, nous donne que la dérivée de $lV(\cdot)_{(x_i)_{i=1}^N}$ est strictement positive sur $]\lambda^*,+\infty[$ et donc $lV(\cdot)_{(x_i)_{i=1}^N}$ est strictement croissante sur $]\lambda^*,+\infty[$

On en conlut que $\lambda^*$ est un minimum de la log-vraisemblance et donc le maximum de vraisemblance de la loi exponentielle.
