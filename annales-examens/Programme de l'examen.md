
# Programme de l'examen:

Pour précision, le TAL (tokenization, vectorization en sac de mot (2 versions) ou avec TF-IDF), a été vu en CM (2 séances) et en TD (1 séance), et est donc évidemment au programme de l'examen écrit.

De même, en 2024 nous avons vu en cours:
- l'algorithme (supervisé) des K-plus proches voisins (K-Nearest-Neighbors ou KNN), qui est très simple
- l'algorithme (non supervisé) de clustering dit des K-moyennes (K-means), qui est un chouilla moins trivial, mais que nous avons bien discuté en CM.

Des 2 algos sont au programme de l'examen.

En revanche, les notions telles que MLP (multi layer perceptron) et word2vec sont juste des ouvertures, et ne sont pas au programme de l'examen.


Ce qui fait foi pour savoir si qqchose est au programme, c'est le fait que ça ait été discuté en CM (voir les slides) et/ou fait en TD/TP (voir, de nouveau, le matériel déposé dans chaque dossier).

Rappel: **vous pouvez amener 6 pages (3 feuilles recto+verso) de notes manuscrites !!**

