# IAS
=====

Ceci est la page web (gitlab) du cours d'Introduction à l'Apprentissage Statistique  (L3, IAS)

# 6 mars - consultation des copies

j'organise une séance de consultation des copies de IAS le **jeudi 6 mars de 13h à 14h en salle C105.**
Essayez de ne pas venir tous en même temps sinon c'est le chaos.

J'en profite pour mentionner que le corrigé est sur le gitlab.

Aussi, le détail des points par question peut etre rendu accessible, il faut juste que j'arrive a le partager de façon anonyme.

# TD de Soutien session 1 - 12 décembre

Après moult atermoiements:

Le **TD de soutien de IAS** aura lieu ce **jeudi 12 décembre** après midi, entre 14h et 16h (comptez p-e même 14h15-16h15 en fait).
La salle sera: **E105**

J'animerai la séance moi même.

François Landes

# TP noté

Copies anonymisées (si vous souhaitez voir ce que vous aviez rendu), pur ceux qui ont respecté la convention de renommage de fichier.
Pour trouver votre copie il faut se souvenir de votre numéro de poste.

https://nextcloud.lisn.upsaclay.fr/index.php/s/YJkAGcwexif2Lfo

Détails des notes (barème détaillé), anonymisé, pour un bon nombre de salles:

https://nextcloud.lisn.upsaclay.fr/index.php/s/4SXyD4HZExzJBdn

Merci à Théo d'avoir fait tout ça !

# 21 novembre:

Le CM10 était en hybride, il a été enregistré ici:
https://bbb.lisn.upsaclay.fr/playback/presentation/2.3/a14f6156c8e38f42807a275ec468326d2bcf4151-1732194345783

Le TD10 a aussi été fait en visio+ enregistré, pour ~20 étudiants:
https://bbb.lisn.upsaclay.fr/playback/presentation/2.3/ecc47cb20bab7f4bd3ee0c4c7904ca23e1adb443-1732200008366

## Aide en python !!

Si vous n'êtes **pas à l'aise en python et numpy**, vous pouvez vous entrainer en utilisant les ressources suivantes:

- dans ce gitlab, il y a un dossier `/python-help/` avec des ressources, mais je vous recommande en particulier:
- https://www.w3schools.com/python/numpy/default.asp (tutoriel numpy, en anglais) Après avoir lu tout un chapitre, vous pouvez passer les tests: https://www.w3schools.com/python/numpy/numpy_quiz.asp
- un notebook collab interactif: https://colab.research.google.com/github/Bergam0t/numpy-100-colab/blob/master/100_Numpy_exercises.ipynb
- moins interactif, mais pas mal pour s'entrainer seul, quand on commence a etre pas mauvais:
https://www.machinelearningplus.com/python/101-numpy-exercises-python/


Pour python pur, très interactif:
- https://www.france-ioi.org/algo/chapters.php -> il faut s'inscrire mais c'est une asso c'est ok


# séance 6: GROUPE 1:

le groupe 1 n'a pas de TD le matin. Vous avez TP de 14h a 18h comme les autres groupes.

Salle: répartissez vous dans les autres groupes, dont les salles sont:
G2: TP IAS G2/E205 (Info)       François Landes
G3: TP IAS G3/E204 (Info)       You Zuo
G4: TP IAS G4/E203 (Miage)      Théo Rudkiewicz
G5: TP IAS G5/D202 (IM LDD)     Nicolas Béreux
G6: TP IAS G6/D104 (Magistère)  Sébastien Velut

----------

Pour télécharger ce repo (repository, dossier distant), faites:
`git clone https://gitlab.inria.fr/flandes/ias.git`

Pour le mettre à jour (car des corrigés ou autres fichiers seront progressivement ajoutés/modifiés)
`git pull`
depuis le dossier où vous avez cloné. (Ainsi, vous ne téléchargez que les différences, c'est plus écolo).

Licence du poly de **cours/TD**: [CC-BY-SA] https://fr.wikipedia.org/wiki/Licence_Creative_Commons

Ce dossier contient/contiendra tous les supports liés au cours, dont:
- le pdf du CM lui même: dans la racine du dossier, ici même
- le pdf des TD/TP (c'est le même fichier pdf)
- les Notebooks qui servent de support au poly (dossiers CM*)
- les Notebooks qui servent d'énoncés de TP    (dossiers TD*)

Les jeux de données utiles pour le cours sont plutôt situées ici (car mutualisées entre plusieurs cours):
https://gitlab.inria.fr/flandes/data-for-teaching

e-campus du cours:
https://ecampus.paris-saclay.fr/ [TBA]

Par défaut, la salle de TP/TD est **la salle de TP**

# Actus

Je me suis trompé, les MCC c'est 30% TP noté et 70% Examen écrit (session 1)

Et en session 2: 20% - 80%, comme j'avais dit.


# Déroulé des séances

voir les dossiers: ils sont numérotés par séance.

CM12 est le contenu du 12ème Cours Magistral.

TD4 est le contenu du 4ème TD/TP.

-------

# Programme de l'examen:

Pour précision, le TAL (tokenization, vectorization en sac de mot (2 versions) ou avec TF-IDF), a été vu en CM (2 séances) et en TD (1 séance), et est donc évidemment au programme de l'examen écrit.

De même, en 2024 nous avons vu en cours:
- l'algorithme (supervisé) des K-plus proches voisins (K-Nearest-Neighbors ou KNN), qui est très simple
- l'algorithme (non supervisé) de clustering dit des K-moyennes (K-means), qui est un chouilla moins trivial, mais que nous avons bien discuté en CM.

Des 2 algos sont au programme de l'examen.

En revanche, les notions telles que MLP (multi layer perceptron) et word2vec sont juste des ouvertures, et ne sont pas au programme de l'examen.


Ce qui fait foi pour savoir si qqchose est au programme, c'est le fait que ça ait été discuté en CM (voir les slides) et/ou fait en TD/TP (voir, de nouveau, le matériel déposé dans chaque dossier).

Rappel: **vous pouvez amener 6 pages (3 feuilles recto+verso) de notes manuscrites !!**


