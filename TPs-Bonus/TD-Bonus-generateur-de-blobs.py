import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import sklearn
from sklearn import datasets

from exo14plots import Deux_paquets_gaussiens


seed=42
np.random.seed(seed) # pour la reproductibilite des data sets

def Deux_paquets_gaussiens(N,D):
    N1 = N // 4 ## division entière
    N2 = N - N1
    X = np.zeros((N,D))

    distanceManhattanEntreLes2Paquets = 4
    mu1 = 0
    sigma1 = 1.5
    mu2 = mu1 + distanceManhattanEntreLes2Paquets
    sigma2 = 1.5
    X[:N1] = np.random.normal(mu1,sigma1, (N1,D))
    X[N1:] = np.random.normal(mu2,sigma2, (N2,D))
    y = np.ones(N)
    y[:N1] = -1 # broadcasting ! :)
    return X,y

def Deux_paquets_gaussiens_separables(N,D):
    X,y = Deux_paquets_gaussiens(N,D)
    barycentre1 = np.mean(X[y==-1])
    barycentre2 = np.mean(X[y== 1])
    squaredDist1 = np.sum((X-barycentre1)**2, axis=1)
    squaredDist2 = np.sum((X-barycentre2)**2, axis=1)
    masque = np.arange(N)[(squaredDist1 - squaredDist2)*y > 0 ]
    X = X[masque]
    y = y[masque]
    Neffectif = X.shape[0]
    return X,y, Neffectif

def ajouter_outliers(Xorigin,yorigin,Nadd):
    N = Xorigin.shape[0]
    D = Xorigin.shape[1]
    X = np.zeros( (N+Nadd, D))
    y = np.zeros( (N+Nadd) )
    X[:N]  = Xorigin.copy()
    y[:N]  = yorigin.copy()

    mu0 = X.mean(axis=0)
    sigma0 = X.std(axis=0)
    X[N:] = (np.random.random( (Nadd, D))-0.5) * sigma0*10 + mu0 ## operation vectorielle
    y[N:] = np.array(np.random.random(Nadd)*2, dtype=int)*2-1
    return X,y

def Deux_paquets_gaussiensNonSymmetriques(N,D):
    N1 = N // 2 ## division entière
    N2 = N - N1
    X = np.zeros((N,D))

    distanceManhattanEntreLes2Paquets = 3
    mu1 = 0
    sigma1 = 1.5
    mu2 = mu1 + distanceManhattanEntreLes2Paquets
    sigma2 = 2
    X[:N1] = np.random.normal(mu1,sigma1, (N1,D))
    X[:N1,0] *=0.5
    X[:N1,1] *=1.5
    X[:N1,1] -=2
    X[N1:] = np.random.normal(mu2,sigma2, (N2,D))
    X[N1:,1] *=0.3
    y = np.ones(N)
    y[:N1] = -1 # broadcasting ! :)
    return X,y

method ="Deux_paquets_gaussiens"
method ="Deux_paquets_gaussiens_separables"
method ="sklearn-gaussians"
method ="sklearn-moons"
method ="Deux_paquets_gaussiensNonSymmetriques"

modificateur="_ajoutOutliers_"
modificateur=""

N = 2000
D = 2
if method =="Deux_paquets_gaussiens" :
    X, y = Deux_paquets_gaussiens(N,D)
if method =="Deux_paquets_gaussiens_separables" :
    X, y,N = Deux_paquets_gaussiens_separables(N,D)
if method =="sklearn-gaussians" :
    X, y = sklearn.datasets.make_blobs(n_samples=N, n_features=D, centers=2, cluster_std=1.5, center_box=(-10.0, 10.0), shuffle=False, random_state=seed)
    y*= 2
    y-=1
if method =="sklearn-moons" :
    noise=0.1
    X, y = sklearn.datasets.make_moons(n_samples=N, shuffle=False, noise=noise, random_state=seed)
    y*= 2
    y-=1
if method =="Deux_paquets_gaussiensNonSymmetriques" :
    X, y = Deux_paquets_gaussiensNonSymmetriques(N,D)


if modificateur == "_ajoutOutliers_":
    X, y  = ajouter_outliers(X,y, 10)

fig1=plt.figure(1)
ax = fig1.add_axes([0.1,0.1,0.9,0.9])
ax.set_aspect(1)
plt.scatter(X[:,0], X[:,1], marker='x', color='black')
plt.savefig(method+modificateur+"black_N="+str(N)+".svg")

plt.figure(2)
ax = fig1.add_axes([0.1,0.1,0.9,0.9])
ax.set_aspect(1)
# plt.xlim()
# plt.ylim()
plt.scatter(X[y==1,0], X[y==1,1], marker='o', color='red')
plt.scatter(X[y==-1,0], X[y==-1,1], marker='x', color='blue')
plt.savefig(method+modificateur+"bicolor_N="+str(N)+".svg")
plt.show()


