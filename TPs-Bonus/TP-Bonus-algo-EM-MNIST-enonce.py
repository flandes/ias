import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
plt.ion()

## structure de données:
## TODO: reflechissez bien au structures des arrays dont vous aurez besoin
## notez ici les noms des arrays que vous utiliserez et leurs dimensions


## TODO: fonction d'apprentissage
## TODO: y ajouter le suivi de la log-vraisemblance


## TODO: fonction de prediction/decision



######### les données ################
import sklearn.datasets
X, _ = sklearn.datasets.load_digits(n_class=K, return_X_y=True)
del  _  ## ne trichez pas !! ##

# ## si sklearn n'est pas installé: ne perdez pas de temps à l'installer, et à la place utilisez:
# LoadObject = np.load("ManualLoad.npz")
# X = LoadObject['X']
# del LoadObject

# LoadObject = np.load("mnist70.npz")
# X = LoadObject['X']
# del LoadObject
######################################


## petite demo d'affichage ##
plt.figure()
n=41 # image numero 42
plt.imshow(X[n].reshape(8,8) , cm.gray)
plt.title("ceci est censé ressembler à un chiffre, avant la binarisation")
# plt.show() ## inutile si on a mis plt.in() au début #
#############################


##################################################
#### preparation des donnees (pre processing) ####

## TODO: un peu de pre processing, si besoin


##################################################
N = X.shape[0]
D = X.shape[1]
K = ?? ##  TODO: a vous de voir

## TODO: entrainer le modele

## TODO: predire des resultats sur des donnees,

###############################
## affichage des parametres: ##
## TODO: voir si les predictions sont satisfaisantes ##
## en particulier, afficher les parametres appris de façon lisible pour un humain
plt.figure()
###############################

## TODO: bonus: passer en semi-supervise ##
## + charger les donnees necessaires
## + definir un test set (tous les labels seront alors dispos)
## + mesurer la performance

## TOOD: faire varier K et refaire tout l'analyse pour chaque valeur.
## qu'observez vous? Pourquoi ?

##################################################


plt.show()
